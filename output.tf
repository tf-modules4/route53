output "pvt_hosted_zone_id" {
  description = "hosted zone id"
  value       = aws_route53_zone.private_hosted_zone.zone_id
}

output "route53_zone_id" {
  value = aws_route53_zone.private_hosted_zone.id
}

output "route53_name" {
  value = aws_route53_zone.private_hosted_zone.name
}