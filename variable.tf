variable "region" {
  type        = string
  description = "Region"
  default     = "ap-south-1"
}

variable "vpc_id" {}

variable "pvt_zone_name" {
  description = "Name of private zone"
  type        = string
}